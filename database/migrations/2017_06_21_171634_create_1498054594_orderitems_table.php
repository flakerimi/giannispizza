<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1498054594OrderitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('orderitems')) {
            Schema::create('orderitems', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned()->nullable();
                $table->foreign('product_id', '47355_594a7fc2c8aca')->references('id')->on('products')->onDelete('cascade');
                $table->decimal('price', 15, 2)->nullable();
                $table->integer('order_id')->unsigned()->nullable();
                $table->foreign('order_id', '47355_594a7fc2d874b')->references('id')->on('orders')->onDelete('cascade');
                
                $table->timestamps();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderitems');
    }
}
