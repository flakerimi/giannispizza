<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1497968415OrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('orders')) {
            Schema::create('orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('client_id')->unsigned()->nullable();
                $table->foreign('client_id', '46980_59492f1f42f4d')->references('id')->on('users')->onDelete('cascade');
                $table->datetime('date')->nullable();
                $table->integer('status_id')->unsigned()->nullable();
                $table->foreign('status_id', '46980_59492f1f488be')->references('id')->on('attribute_items')->onDelete('cascade');
                $table->integer('delivered_by_id')->unsigned()->nullable();
                $table->foreign('delivered_by_id', '46980_59492f1f5139e')->references('id')->on('users')->onDelete('cascade');
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
