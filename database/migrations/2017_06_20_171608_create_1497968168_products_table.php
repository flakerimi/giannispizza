<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1497968168ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('products')) {
            Schema::create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->integer('round_price')->nullable();
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '46978_59492e28edaeb')->references('id')->on('users')->onDelete('cascade');
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '46978_59492e28f225a')->references('id')->on('users')->onDelete('cascade');
                
                $table->timestamps();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
