<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1498057211ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('secondary_ingredients_id')->unsigned()->nullable();
                $table->foreign('secondary_ingredients_id', '46978_594a89faae0fd')->references('id')->on('attribute_items')->onDelete('cascade');
                
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('46978_594a89faae0fd');
            $table->dropIndex('46978_594a89faae0fd');
            $table->dropColumn('secondary_ingredients_id');
            
        });

    }
}
