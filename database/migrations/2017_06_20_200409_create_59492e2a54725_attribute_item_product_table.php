<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create59492e2a54725AttributeItemProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('attribute_item_product')) {
            Schema::create('attribute_item_product', function (Blueprint $table) {
                $table->integer('attribute_item_id')->unsigned()->nullable();
                $table->foreign('attribute_item_id', 'fk_p_46977_46978_product__59492e2a54925')->references('id')->on('attribute_items')->onDelete('cascade');
                $table->integer('product_id')->unsigned()->nullable();
                $table->foreign('product_id', 'fk_p_46978_46977_attribut_59492e2a549e2')->references('id')->on('products')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_item_product');
    }
}
