<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1497968598OrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('address_id')->unsigned()->nullable();
                $table->foreign('address_id', '46980_59492fd540b73')->references('id')->on('addresses')->onDelete('cascade');
                
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('46980_59492fd540b73');
            $table->dropIndex('46980_59492fd540b73');
            $table->dropColumn('address_id');
            
        });

    }
}
