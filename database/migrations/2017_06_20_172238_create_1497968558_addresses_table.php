<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1497968558AddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('addresses')) {
            Schema::create('addresses', function (Blueprint $table) {
                $table->increments('id');
                $table->text('address')->nullable();
                $table->string('city')->nullable();
                $table->string('longtitude')->nullable();
                $table->string('latitude')->nullable();
                $table->integer('client_id')->unsigned()->nullable();
                $table->foreign('client_id', '46981_59492fae42f1e')->references('id')->on('users')->onDelete('cascade');
                
                $table->timestamps();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
