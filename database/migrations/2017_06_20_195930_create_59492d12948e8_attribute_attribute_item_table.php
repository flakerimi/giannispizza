<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create59492d12948e8AttributeAttributeItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('attribute_attribute_item')) {
            Schema::create('attribute_attribute_item', function (Blueprint $table) {
                $table->integer('attribute_id')->unsigned()->nullable();
                $table->foreign('attribute_id', 'fk_p_46976_46977_attribut_59492d12949ce')->references('id')->on('attributes')->onDelete('cascade');
                $table->integer('attribute_item_id')->unsigned()->nullable();
                $table->foreign('attribute_item_id', 'fk_p_46977_46976_attribut_59492d1294a56')->references('id')->on('attribute_items')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_attribute_item');
    }
}
