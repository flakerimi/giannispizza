<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => '$2y$10$vDISx6ouht010SwHd9qdQuztmMqiS9EIvJ5AH.W4vgA.YZ1rQRzM2', 'role_id' => 1, 'remember_token' => '',],
            ['id' => 2, 'name' => 'Manager', 'email' => 'manager@pizza.app', 'password' => '$2y$10$i5GktZ2ey15sDObFBO7jI.3LfC9/SrSuY1NqOiS94fZ1e95a14hH.', 'role_id' => 2, 'remember_token' => null,],
            ['id' => 3, 'name' => 'Client', 'email' => 'client@pizza.app', 'password' => '$2y$10$IfdYlk47ix1MC5HXL/ishudc3bilKx71cxoI033o4Q9cN.F3/M0tu', 'role_id' => 3, 'remember_token' => null,],

        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
