<?php

use Illuminate\Database\Seeder;

class AttributeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'Pizza Primary Ingredients', 'slug' => 'pizza-primary-ingredients',],
            ['id' => 2, 'name' => 'Pizza Secondary Ingredients', 'slug' => 'pizza-secondary-ingredients',],
            ['id' => 3, 'name' => 'Status', 'slug' => 'status',],

        ];

        foreach ($items as $item) {
            \App\Attribute::create($item);
        }

    }
}
