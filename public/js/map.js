$(document).ready(function(){

	$('.location').click(function(){
		fillAddress();
	});
});


var map;
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var labelIndex = 0;

var markers = [];

var latitude, longtitude;

function initMap() {
	var pos = { lat:  42.6582036, lng: 21.1315677};

    map = new google.maps.Map(document.getElementById('map'), {
      center: pos,
      zoom: 12
    });
	addMarker(pos, map);

	google.maps.event.addListener(map, 'rightclick', function(event){
		addMarker(event.latLng, map);
		latitude = event.latLng.lat();
		longtitude = event.latLng.lng();

		console.log(event);
		console.log(latitude + " " + longtitude);
	});
	/*var request = {
	    location: pos,
	    radius: '1400',
	    type: ['restaurant']
	  };

	var service = new google.maps.places.PlacesService(map);
	service.nearbySearch(request, callback);*/
}
function callback(results, status) {
	if (status == google.maps.places.PlacesServiceStatus.OK) {
		for (var i = 0; i < results.length; i++) {
			var place = results[i];
			let latlng = place.geometry.location;
			let icn = place.icon;
			//console.log(latlng);
			createMarker(latlng, icn);
		}
	}
}
// function createMarker(latlng, icn){
// 	var marker = new google.maps.Marker({
// 		position: latlng,
// 		map: map,
// 		icon: icn,
// 		title: "Hello world"
// 	});
// }

function addMarker(location, map){
//	console.log(location.lat);
	//console.log(location.lat.Scopes + " : "+ location.lng.Scopes);
	var marker = new google.maps.Marker({
		position: location,
		//label: labels[labelIndex++ % labels.length],
		map: map,
		draggable: true,
      	animation: google.maps.Animation.DROP,
	});
	markers.push(marker);
}

function setMapOnAll(map){
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}
function clearMarkers(){
	setMapOnAll(null);
}

function showMarkers(){
	setMapOnAll(map);
}

function deleteMarkers(){
	clearMarkers();
	markers = [];
	longtitude = null;
	latitude = null;
}

function fillAddress(){
	let street = $('#address').val();
	let number = $('#number').val();
	let city = $('#city').val();

	var data = {
		method : "POST",
		url: '/address/store',
		latitude :  (latitude != undefined)? latitude : null,
		longtitude : (longtitude != undefined)? longtitude : null
	};

	if(street && number && city)
	{
		data.address = street + "_" + number;
		data.city = city;

		storeData(data);
	}
	else if(latitude != undefined && longtitude != undefined)
	{
		// here we will grab city and street from geocode api maps.google
		data.address = "Something";
		data.city = "Prishtine";

		storeData(data);
	}
	else
	{
		alert('Please pick your location or fill fields');
	}
}
function storeData(data)
{
	$.ajax({
		type: data.method,
		url: data.url,
		data: {longtitude: data.longtitude, latitude: data.latitude, address: data.address, city: data.city},
		success: function(status){
			location.href = "/address";
		}
	});
}
