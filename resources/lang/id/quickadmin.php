<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',		],	],
		'user-actions' => [		'title' => 'User actions',		'created_at' => 'Time',		'fields' => [			'user' => 'User',			'action' => 'Action',			'action-model' => 'Action model',			'action-id' => 'Action id',		],	],
		'internal-notifications' => [		'title' => 'Notifications',		'created_at' => 'Time',		'fields' => [			'text' => 'Text',			'link' => 'Link',			'users' => 'Users',		],	],
		'catalogue' => [		'title' => 'Catalogue',		'created_at' => 'Time',		'fields' => [		],	],
		'attributes' => [		'title' => 'Attributes',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'slug' => 'Slug',		],	],
		'attribute-items' => [		'title' => 'Attribute items',		'created_at' => 'Time',		'fields' => [			'attribute' => 'Attribute',			'name' => 'Name',			'value' => 'Value',			'photo' => 'Photo',		],	],
		'products' => [		'title' => 'Products',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'round-price' => 'Round price',			'attributes' => 'Attributes',			'primary-ingredients' => 'Primary ingredients',			'secondary-ingredients' => 'Secondary ingredients',			'user' => 'Created by',		],	],
		'sales' => [		'title' => 'Sales',		'created_at' => 'Time',		'fields' => [		],	],
		'orders' => [		'title' => 'Orders',		'created_at' => 'Time',		'fields' => [			'client' => 'Client',			'date' => 'Date',			'status' => 'Status',			'delivered-by' => 'Delivered by',			'address' => 'Address',		],	],
		'addresses' => [		'title' => 'Addresses',		'created_at' => 'Time',		'fields' => [			'address' => 'Address',			'city' => 'City',			'longtitude' => 'Longtitude',			'latitude' => 'Latitude',			'client' => 'Client',		],	],
		'orderitems' => [		'title' => 'Order items',		'created_at' => 'Time',		'fields' => [			'product' => 'Product',			'price' => 'Price',			'order' => 'Order',		],	],
	'qa_create' => 'Buat',
	'qa_save' => 'Simpan',
	'qa_edit' => 'Edit',
	'qa_view' => 'Lihat',
	'qa_update' => 'Update',
	'qa_list' => 'Daftar',
	'qa_no_entries_in_table' => 'Tidak ada data di tabel',
	'custom_controller_index' => 'Controller index yang sesuai kebutuhan Anda.',
	'qa_logout' => 'Keluar',
	'qa_add_new' => 'Tambahkan yang baru',
	'qa_are_you_sure' => 'Anda yakin?',
	'qa_back_to_list' => 'Kembali ke daftar',
	'qa_dashboard' => 'Dashboard',
	'qa_delete' => 'Hapus',
	'quickadmin_title' => 'GiannisPizza',
];