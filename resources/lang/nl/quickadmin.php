<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',		],	],
		'user-actions' => [		'title' => 'User actions',		'created_at' => 'Time',		'fields' => [			'user' => 'User',			'action' => 'Action',			'action-model' => 'Action model',			'action-id' => 'Action id',		],	],
		'internal-notifications' => [		'title' => 'Notifications',		'created_at' => 'Time',		'fields' => [			'text' => 'Text',			'link' => 'Link',			'users' => 'Users',		],	],
		'catalogue' => [		'title' => 'Catalogue',		'created_at' => 'Time',		'fields' => [		],	],
		'attributes' => [		'title' => 'Attributes',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'slug' => 'Slug',		],	],
		'attribute-items' => [		'title' => 'Attribute items',		'created_at' => 'Time',		'fields' => [			'attribute' => 'Attribute',			'name' => 'Name',			'value' => 'Value',			'photo' => 'Photo',		],	],
		'products' => [		'title' => 'Products',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'round-price' => 'Round price',			'attributes' => 'Attributes',			'primary-ingredients' => 'Primary ingredients',			'secondary-ingredients' => 'Secondary ingredients',			'user' => 'Created by',		],	],
		'sales' => [		'title' => 'Sales',		'created_at' => 'Time',		'fields' => [		],	],
		'orders' => [		'title' => 'Orders',		'created_at' => 'Time',		'fields' => [			'client' => 'Client',			'date' => 'Date',			'status' => 'Status',			'delivered-by' => 'Delivered by',			'address' => 'Address',		],	],
		'addresses' => [		'title' => 'Addresses',		'created_at' => 'Time',		'fields' => [			'address' => 'Address',			'city' => 'City',			'longtitude' => 'Longtitude',			'latitude' => 'Latitude',			'client' => 'Client',		],	],
		'orderitems' => [		'title' => 'Order items',		'created_at' => 'Time',		'fields' => [			'product' => 'Product',			'price' => 'Price',			'order' => 'Order',		],	],
	'qa_create' => 'Toevoegen',
	'qa_save' => 'Opslaan',
	'qa_edit' => 'Bewerken',
	'qa_view' => 'Bekijken',
	'qa_update' => 'Bijwerken',
	'qa_list' => 'Lijst',
	'qa_no_entries_in_table' => 'Geen inhoud gevonden',
	'custom_controller_index' => 'Custom controller index.',
	'qa_logout' => 'Logout',
	'qa_add_new' => 'Toevoegen',
	'qa_are_you_sure' => 'Ben je zeker?',
	'qa_back_to_list' => 'Terug naar lijst',
	'qa_dashboard' => 'Boordtabel',
	'qa_delete' => 'Verwijderen',
	'quickadmin_title' => 'GiannisPizza',
];