@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.attribute-items.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.attribute-items.fields.attribute')</th>
                            <td>
                                @foreach ($attribute_item->attribute as $singleAttribute)
                                    <span class="label label-info label-many">{{ $singleAttribute->name }}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.attribute-items.fields.name')</th>
                            <td>{{ $attribute_item->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.attribute-items.fields.value')</th>
                            <td>{{ $attribute_item->value }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.attribute-items.fields.photo')</th>
                            <td>@if($attribute_item->photo)<a href="{{ asset('uploads/' . $attribute_item->photo) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $attribute_item->photo) }}"/></a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Products</a></li>
<li role="presentation" class=""><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">Orders</a></li>
<li role="presentation" class=""><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Products</a></li>
<li role="presentation" class=""><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Products</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="products">
<table class="table table-bordered table-striped {{ count($products) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.products.fields.name')</th>
                        <th>@lang('quickadmin.products.fields.round-price')</th>
                        <th>@lang('quickadmin.products.fields.attributes')</th>
                        <th>@lang('quickadmin.products.fields.primary-ingredients')</th>
                        <th>@lang('quickadmin.products.fields.secondary-ingredients')</th>
                        <th>@lang('quickadmin.products.fields.user')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($products) > 0)
            @foreach ($products as $product)
                <tr data-entry-id="{{ $product->id }}">
                    <td>{{ $product->name }}</td>
                                <td>{{ $product->round_price }}</td>
                                <td>
                                    @foreach ($product->attributes as $singleAttributes)
                                        <span class="label label-info label-many">{{ $singleAttributes->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($product->primary_ingredients as $singlePrimaryIngredients)
                                        <span class="label label-info label-many">{{ $singlePrimaryIngredients->name }}</span>
                                    @endforeach
                                </td>
                                <td>{{ $product->secondary_ingredients->name or '' }}</td>
                                <td>{{ $product->user->name or '' }}</td>
                                <td>
                                    @can('product_view')
                                    <a href="{{ route('admin.products.show',[$product->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('product_edit')
                                    <a href="{{ route('admin.products.edit',[$product->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('product_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.products.destroy', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="orders">
<table class="table table-bordered table-striped {{ count($orders) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.orders.fields.client')</th>
                        <th>@lang('quickadmin.orders.fields.date')</th>
                        <th>@lang('quickadmin.orders.fields.status')</th>
                        <th>@lang('quickadmin.orders.fields.delivered-by')</th>
                        <th>@lang('quickadmin.orders.fields.address')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($orders) > 0)
            @foreach ($orders as $order)
                <tr data-entry-id="{{ $order->id }}">
                    <td>{{ $order->client->name or '' }}</td>
                                <td>{{ $order->date }}</td>
                                <td>{{ $order->status->name or '' }}</td>
                                <td>{{ $order->delivered_by->name or '' }}</td>
                                <td>{{ $order->address->address or '' }}</td>
                                <td>
                                    @can('order_view')
                                    <a href="{{ route('admin.orders.show',[$order->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('order_edit')
                                    <a href="{{ route('admin.orders.edit',[$order->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('order_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.orders.destroy', $order->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="products">
<table class="table table-bordered table-striped {{ count($products) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.products.fields.name')</th>
                        <th>@lang('quickadmin.products.fields.round-price')</th>
                        <th>@lang('quickadmin.products.fields.attributes')</th>
                        <th>@lang('quickadmin.products.fields.primary-ingredients')</th>
                        <th>@lang('quickadmin.products.fields.secondary-ingredients')</th>
                        <th>@lang('quickadmin.products.fields.user')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($products) > 0)
            @foreach ($products as $product)
                <tr data-entry-id="{{ $product->id }}">
                    <td>{{ $product->name }}</td>
                                <td>{{ $product->round_price }}</td>
                                <td>
                                    @foreach ($product->attributes as $singleAttributes)
                                        <span class="label label-info label-many">{{ $singleAttributes->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($product->primary_ingredients as $singlePrimaryIngredients)
                                        <span class="label label-info label-many">{{ $singlePrimaryIngredients->name }}</span>
                                    @endforeach
                                </td>
                                <td>{{ $product->secondary_ingredients->name or '' }}</td>
                                <td>{{ $product->user->name or '' }}</td>
                                <td>
                                    @can('product_view')
                                    <a href="{{ route('admin.products.show',[$product->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('product_edit')
                                    <a href="{{ route('admin.products.edit',[$product->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('product_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.products.destroy', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="products">
<table class="table table-bordered table-striped {{ count($products) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.products.fields.name')</th>
                        <th>@lang('quickadmin.products.fields.round-price')</th>
                        <th>@lang('quickadmin.products.fields.attributes')</th>
                        <th>@lang('quickadmin.products.fields.primary-ingredients')</th>
                        <th>@lang('quickadmin.products.fields.secondary-ingredients')</th>
                        <th>@lang('quickadmin.products.fields.user')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($products) > 0)
            @foreach ($products as $product)
                <tr data-entry-id="{{ $product->id }}">
                    <td>{{ $product->name }}</td>
                                <td>{{ $product->round_price }}</td>
                                <td>
                                    @foreach ($product->attributes as $singleAttributes)
                                        <span class="label label-info label-many">{{ $singleAttributes->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($product->primary_ingredients as $singlePrimaryIngredients)
                                        <span class="label label-info label-many">{{ $singlePrimaryIngredients->name }}</span>
                                    @endforeach
                                </td>
                                <td>{{ $product->secondary_ingredients->name or '' }}</td>
                                <td>{{ $product->user->name or '' }}</td>
                                <td>
                                    @can('product_view')
                                    <a href="{{ route('admin.products.show',[$product->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('product_edit')
                                    <a href="{{ route('admin.products.edit',[$product->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('product_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.products.destroy', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.attribute_items.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop