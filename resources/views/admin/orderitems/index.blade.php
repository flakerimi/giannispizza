@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.orderitems.title')</h3>
    @can('orderitem_create')
    <p>
        <a href="{{ route('admin.orderitems.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($orderitems) > 0 ? 'datatable' : '' }} @can('orderitem_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('orderitem_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.orderitems.fields.product')</th>
                        <th>@lang('quickadmin.orderitems.fields.price')</th>
                        <th>@lang('quickadmin.orderitems.fields.order')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($orderitems) > 0)
                        @foreach ($orderitems as $orderitem)
                            <tr data-entry-id="{{ $orderitem->id }}">
                                @can('orderitem_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $orderitem->product->name or '' }}</td>
                                <td>{{ $orderitem->price }}</td>
                                <td>{{ $orderitem->order->date or '' }}</td>
                                <td>
                                    @can('orderitem_view')
                                    <a href="{{ route('admin.orderitems.show',[$orderitem->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('orderitem_edit')
                                    <a href="{{ route('admin.orderitems.edit',[$orderitem->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('orderitem_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.orderitems.destroy', $orderitem->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('orderitem_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.orderitems.mass_destroy') }}';
        @endcan

    </script>
@endsection