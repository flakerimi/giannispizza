@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.orderitems.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.orderitems.fields.product')</th>
                            <td>{{ $orderitem->product->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orderitems.fields.price')</th>
                            <td>{{ $orderitem->price }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orderitems.fields.order')</th>
                            <td>{{ $orderitem->order->date or '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.orderitems.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop