@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.addresses.title')</h3>
    @can('address_create')
    <p>
        <a href="{{ route('admin.addresses.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('address_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('address_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.addresses.fields.address')</th>
                        <th>@lang('quickadmin.addresses.fields.city')</th>
                        <th>@lang('quickadmin.addresses.fields.longtitude')</th>
                        <th>@lang('quickadmin.addresses.fields.latitude')</th>
                        <th>@lang('quickadmin.addresses.fields.client')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('address_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.addresses.mass_destroy') }}';
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.addresses.index') !!}';
            window.dtDefaultOptions.columns = [
                @can('address_delete')
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endcan
                {data: 'address', name: 'address'},
                {data: 'city', name: 'city'},
                {data: 'longtitude', name: 'longtitude'},
                {data: 'latitude', name: 'latitude'},
                {data: 'client.name', name: 'client.name'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection