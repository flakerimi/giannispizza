@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.addresses.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.addresses.fields.address')</th>
                            <td>{!! $address->address !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.addresses.fields.city')</th>
                            <td>{{ $address->city }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.addresses.fields.longtitude')</th>
                            <td>{{ $address->longtitude }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.addresses.fields.latitude')</th>
                            <td>{{ $address->latitude }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.addresses.fields.client')</th>
                            <td>{{ $address->client->name or '' }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">Orders</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="orders">
<table class="table table-bordered table-striped {{ count($orders) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.orders.fields.client')</th>
                        <th>@lang('quickadmin.orders.fields.date')</th>
                        <th>@lang('quickadmin.orders.fields.status')</th>
                        <th>@lang('quickadmin.orders.fields.delivered-by')</th>
                        <th>@lang('quickadmin.orders.fields.address')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($orders) > 0)
            @foreach ($orders as $order)
                <tr data-entry-id="{{ $order->id }}">
                    <td>{{ $order->client->name or '' }}</td>
                                <td>{{ $order->date }}</td>
                                <td>{{ $order->status->name or '' }}</td>
                                <td>{{ $order->delivered_by->name or '' }}</td>
                                <td>{{ $order->address->address or '' }}</td>
                                <td>
                                    @can('order_view')
                                    <a href="{{ route('admin.orders.show',[$order->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('order_edit')
                                    <a href="{{ route('admin.orders.edit',[$order->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('order_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.orders.destroy', $order->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.addresses.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop