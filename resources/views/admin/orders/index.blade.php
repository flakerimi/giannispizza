@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.orders.title')</h3>
    @can('order_create')
    <p>
        <a href="{{ route('admin.orders.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('order_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('order_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.orders.fields.client')</th>
                        <th>@lang('quickadmin.orders.fields.date')</th>
                        <th>@lang('quickadmin.orders.fields.status')</th>
                        <th>@lang('quickadmin.orders.fields.delivered-by')</th>
                        <th>@lang('quickadmin.orders.fields.address')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('order_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.orders.mass_destroy') }}';
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.orders.index') !!}';
            window.dtDefaultOptions.columns = [
                @can('order_delete')
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endcan
                {data: 'client.name', name: 'client.name'},
                {data: 'date', name: 'date'},
                {data: 'status.name', name: 'status.name'},
                {data: 'delivered_by.name', name: 'delivered_by.name'},
                {data: 'address.address', name: 'address.address'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection