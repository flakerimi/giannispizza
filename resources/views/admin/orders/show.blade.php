@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.orders.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.orders.fields.client')</th>
                            <td>{{ $order->client->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.date')</th>
                            <td>{{ $order->date }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.status')</th>
                            <td>{{ $order->status->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.delivered-by')</th>
                            <td>{{ $order->delivered_by->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.address')</th>
                            <td>{{ $order->address->address or '' }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#orderitems" aria-controls="orderitems" role="tab" data-toggle="tab">Order items</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="orderitems">
<table class="table table-bordered table-striped {{ count($orderitems) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.orderitems.fields.product')</th>
                        <th>@lang('quickadmin.orderitems.fields.price')</th>
                        <th>@lang('quickadmin.orderitems.fields.order')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($orderitems) > 0)
            @foreach ($orderitems as $orderitem)
                <tr data-entry-id="{{ $orderitem->id }}">
                    <td>{{ $orderitem->product->name or '' }}</td>
                                <td>{{ $orderitem->price }}</td>
                                <td>{{ $orderitem->order->date or '' }}</td>
                                <td>
                                    @can('orderitem_view')
                                    <a href="{{ route('admin.orderitems.show',[$orderitem->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('orderitem_edit')
                                    <a href="{{ route('admin.orderitems.edit',[$orderitem->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('orderitem_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.orderitems.destroy', $orderitem->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.orders.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop