@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.attributes.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.attributes.fields.name')</th>
                            <td>{{ $attribute->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.attributes.fields.slug')</th>
                            <td>{{ $attribute->slug }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#attributeitems" aria-controls="attributeitems" role="tab" data-toggle="tab">Attribute items</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="attributeitems">
<table class="table table-bordered table-striped {{ count($attribute_items) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.attribute-items.fields.attribute')</th>
                        <th>@lang('quickadmin.attribute-items.fields.name')</th>
                        <th>@lang('quickadmin.attribute-items.fields.value')</th>
                        <th>@lang('quickadmin.attribute-items.fields.photo')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($attribute_items) > 0)
            @foreach ($attribute_items as $attribute_item)
                <tr data-entry-id="{{ $attribute_item->id }}">
                    <td>
                                    @foreach ($attribute_item->attribute as $singleAttribute)
                                        <span class="label label-info label-many">{{ $singleAttribute->name }}</span>
                                    @endforeach
                                </td>
                                <td>{{ $attribute_item->name }}</td>
                                <td>{{ $attribute_item->value }}</td>
                                <td>@if($attribute_item->photo)<a href="{{ asset('uploads/' . $attribute_item->photo) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $attribute_item->photo) }}"/></a>@endif</td>
                                <td>
                                    @can('attribute_item_view')
                                    <a href="{{ route('admin.attribute_items.show',[$attribute_item->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('attribute_item_edit')
                                    <a href="{{ route('admin.attribute_items.edit',[$attribute_item->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('attribute_item_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.attribute_items.destroy', $attribute_item->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.attributes.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop