@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.attributes.title')</h3>
    @can('attribute_create')
    <p>
        <a href="{{ route('admin.attributes.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($attributes) > 0 ? 'datatable' : '' }} @can('attribute_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('attribute_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.attributes.fields.name')</th>
                        <th>@lang('quickadmin.attributes.fields.slug')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($attributes) > 0)
                        @foreach ($attributes as $attribute)
                            <tr data-entry-id="{{ $attribute->id }}">
                                @can('attribute_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $attribute->name }}</td>
                                <td>{{ $attribute->slug }}</td>
                                <td>
                                    @can('attribute_view')
                                    <a href="{{ route('admin.attributes.show',[$attribute->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('attribute_edit')
                                    <a href="{{ route('admin.attributes.edit',[$attribute->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('attribute_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.attributes.destroy', $attribute->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('attribute_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.attributes.mass_destroy') }}';
        @endcan

    </script>
@endsection