@extends('client.layouts.master')

@section('orderContainer')

    <div class="container">
		<div class="well well-sm">
            <h4>Orders</h4>
        </div>
		<div class="row">
			<div class="col-md-8">
				<div class="panel-group">
					@foreach ($orders as $order)
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse"
									href="#{{ $order->id }}"
									style="text-decoration: none;">
									<h4>
										<small>Name:</small>
										 <a data-toggle="collapse"
											href="#{{ $order->id }}">
												{{ $order->orderitem->product->name }}
										</a>
										<span class="badge pull-right" >{{ $order->created_at->diffForHumans() }}</span>
									</h4>
								</a>
							</div>
							<div id="{{ $order->id}}" class="panel-collapse collapse">
								<div class="panel-body">
									<span>{{ $order->created_at->toFormattedDateString() }}</span>
								</div>
							</div>
						</div>
	        		@endforeach
				</div>
			</div>
			<div class="col-md-3">

			</div>
        </div>
    </div>
@endsection

@section('jquery_part')
    <script>
        $(document).ready(function(){

        });
    </script>
@endsection
