<div id="mapModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-map-marker"></i> Choose location and confirm order </h4>
      </div>
      <div class="modal-body">
          @if (count($address) >= 1)
              <form action="/order/confirm" method="POST">
                  {{ csrf_field() }}
                <p>To make this order posible choose address and confirm order</p>
                <div class="form-group">
                        <label for="address">Address</label>
                        <select class="form-control" name="address">
                            @foreach ($address as $ad)
                                <option value="{{ $ad->id}}">{{ $ad->address }}/ {{ $ad->city}}</option>
                            @endforeach
                        </select>
                </div>
                <button class="btn btn-success" >Confrim Order</button>
            </form>
            @else
                <h4>First create an address</h4>
                <a href="{{ url('address/create')  }}">Create address</a>
            @endif
        </div>
    </div>
  </div>
</div>
