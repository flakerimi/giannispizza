@extends('client.layouts.master')

@section('orderContainer')
	<div class="container border-buttons">
		<div class="row">
			<div id="floating-panel">
		      <input onclick="clearMarkers();" type=button value="Hide Markers">
		      <input onclick="showMarkers();" type=button value="Show All Markers">
		      <input onclick="deleteMarkers();" type=button value="Delete Markers">
		    </div>
			<div class="col-md-6 ">
				<div id="map"></div>
			</div>
			<div class="col-md-6">
				<blockquote>
					<div class="row">
						<div class="col-md-9">
							<h2>Store your location</h2>
							<h3>And you will get product in hand</h3>

						</div>
						<div class="col-md-3">
							<h1><i class="fa fa-map-o fa-2x"></i></h1>
						</div>
					</div>
				</blockquote>
					<p>You can pik your location in map or u just can write address and city correctly.</p>
				<hr>
				<div class="form-group">
					<label for="address">Address</label>
					<div class="input-group">
							<div class="input-group-addon">Str.</div>
							<input type="text" id="address" class="form-control" name="address">
					</div>
				</div>
				<div class="form-group">
					<label for="number">Number</label>
					<div class="input-group">
							<div class="input-group-addon">Num.</div>
							<input type="text" id="number" class="form-control" name="number">
					</div>
				</div>
				<div class="form-group">
					<label for="city">City</label>
					<div class="input-group">

							<div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
							<input type="text" id="city" class="form-control" name="city" >
					</div>
				</div>
				<div class="form-group">
					<a type="button" class="btn btn-success pull-right btn-lg location">Save</a>
				</div>
			</div>
		</div>
	</div>
	<style media="screen">
	#floating-panel {
		position: absolute;
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	  }
	</style>
@endsection

{{-- @section('jquery_part')

	<script src="{{ asset('/js/map.js') }}"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCej9FpDGZCLTq1bEGSG-Gb2eusCQBagaA&libraries=places&callback=initMap"></script>

@endsection --}}
