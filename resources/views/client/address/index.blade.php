@extends('client.layouts.master')

@section('orderContainer')
	<div class="container border-buttons">
		<div class="well well-sm">
			<a class="btn btn-success pull-right " href="{{ url('/address/create')}}" type="button">Add</a>
			<h4 class="text-left">Add your new address</h4>
		</div>
		<div class="row">
			<div class="col-md-3">
				<ul class="nav nav-pills nav-stacked nav-left">
                        <li role="presentation"><a href="#"><i class="fa fa-map-marker fa-lg"></i></a></li>
						<li role="presentation" class="address"><a href="#">Addresses</a></li>
						<li role="presentation" class="map"><a href="#">Maps location</a></li>
                </ul>
			</div>
			<div class="col-md-9">
				<div class="col-md-12 part1">
					<table class="table">
						<thead>
							<tr>
								<td width="3%"></td>
								<td width="45%">Address</td>
								<td width="45%">City</td>
								<td width="7%"></td>
							</tr>
						</thead>
						<tbody class='address-part'>
							@foreach ($addresses as $address)
								<tr>
									<td>{{ $loop->index + 1}}</td>
									<td>{{ $address->address }}</td>
									<td>{{ $address->city }}</td>
									<td>
										<form action="address/delete/{{ $address->id }}" method="DELETE">
											{{ csrf_field() }}
											<button type="submit" class="btn btn-danger">Delete</button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-md-12 part2">
					hello
					{{-- <div id="map"></div> --}}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('jquery_part')
	<script type="text/javascript">
		$(document).ready(function(){

			var part1 = $('.part1');
			var part2 = $('.part2');

			part2.hide();

			 $('.map').click(function(){
				part1.hide();
				part2.show('slow');
			});
			$('.address').click(function(){
				part2.hide();
				part1.show('slow');
			});
		});
	</script>
@endsection
