@extends('client.layouts.master')

@section('orderContainer')


        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-warning" style="border:0px;">
                        <div class="panel-heading">
                            Creat you own pizza by choose what do you want to put on them
                        </div>
                    </div>
                    <form  action="/admin/order/store/custom" method="Post">
                        {{ csrf_field() }}
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="Name">How will you call this:</label>
                                    <input type="text" class="form-control" name="nameProduct">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <table class="table table-default">
                                    <h4>Add: <small>what do you wannt to have your pizza</small></h4>
                                    <thead>
                                        <tr>
                                            <th width="60%">Name</th>
                                            <th width="30%">Price</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ingredients as $ingredient)
                                        <tr>
                                            <td>{{ $ingredient->name }}</td>
                                            <td>
                                                <span>€</span>
                                                {{ $ingredient->value }}
                                            </td>
                                            <td>
                                                <input type="checkbox" name="{{ $ingredient->id }}" value="{{ $ingredient->id }}">
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success ">Make your order</buton>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                    <img style="width: 100px;" src="{{ asset('img/profile.png')}}" >
                                    <span style="maring:0px;" class="text-center"><strong>Name:</strong> Filan Fisteku</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <h2 style="padding:0px;margin:0px;"class="pull-right">
                                    70
                                    <small>minutes</small>
                                </h2>
                                <span style="padding-top: 10px;" class="pull-left">Estimated delivery time</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <h4><img width="30" src="{{ asset('img/pizza-logo-custom.jpg')}}" alt=""> Products <small>you have created</small> </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel-group">
                            {{-- @foreach ($makeyourowns as $makeyourown)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse"
                                            href="#{{ $makeyourown->name}}"
                                            style="text-decoration: none;">
                                            <h4>
                                                <small>Name:</small>
                                                {{-- <a data-toggle="collapse"
                                                    href="#{{ $makeyourown->name}}">
                                                    {{ $makeyourown->name}}
                                                {{-- </a>
                                                <small class="pull-right">Date: {{ $makeyourown->created_at }}</small>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="{{ $makeyourown->name}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Ingredients for this product.
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img width="100%"src="{{ asset('img/dominos-pizza.jpg') }}" alt="">
                                                </div>
                                                <div class="col-md-6">
                                                    <ul>
                                                        <li style="list-style-type:none;">
                                                            Name:
                                                            <span class="pull-right">Price:</span>
                                                        </li>
                                                    </ul>
                                                    <ol style="list-type:none;">
                                                        @foreach($makeyourown->ingridient as $make)
                                                            <li>
                                                                {{ $make->name }} <span class="pull-right">{{ $make->price }}</span>
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <button type="submit" class="btn btn-primary btn-md btn-block" name="button">Reorder</button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection
