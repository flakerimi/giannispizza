<nav class="navbar navbar-default">
       <div class="container">
           <!-- Brand and toggle get grouped for better mobile display -->
           <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                   <span class="sr-only">Toggle navigation</span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="#">Pizza</a>
           </div>

           <!-- Collect the nav links, forms, and other content for toggling -->
           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-right" >
                   <li class="active"><a href="{{ url('/order') }}">Home <span class="sr-only">(current)</span></a></li>
                   <li><a href="{{ url('/product')}}">Products</a></li>
                   <li><a href="#">Profile</a></li>
                   <li><a href="{{ url('/cart')}}"><i class="fa fa-shopping-cart"></i></a></li>
                   <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                           <span class="caret"></span></a>
                       <ul class="dropdown-menu">
                           <li><a href="#"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a></li>
                           <li role="separator" class="divider"></li>

                           <li><a href="{{ url('/myorders')}}"><i class="fa fa-reorder"></i> My Orders</a></li>
                           <li><a href="{{ url('/order/custom/product')}}"  ><i class="fa fa-upload"></i> Custtom product</a></li>
                           <li role="separator" class="divider"></li>

                           <li><a href="#"><i class="fa fa-envelope"></i> Messages</a></li>
                           <li><a href="#"> <i class="fa fa-trophy" aria-hidden="true"></i> &nbsp;Coupons</a></li>
                           <li role="separator" class="divider"></li>

                           <li><a href="{{ url('/address')}}"><i class="fa fa-map"></i> Addresses</a></li>
                           <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                           <li><a href="#logout" onclick="$('#logout').submit();" ><i class="fa fa-sign-out"></i> Log Out</a></li>
                       </ul>
                   </li>
               </ul>
               {{-- <form class="navbar-form navbar-right">
                   <div class="form-group">
                       <input type="text" class="form-control" placeholder="Search">
                   </div>
                   <button type="submit" class="btn btn-warning">Submit</button>
               </form> --}}
           </div><!-- /.navbar-collapse -->
       </div>
   </nav>
   {!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
   <button type="submit">@lang('quickadmin.logout')</button>
   {!! Form::close() !!}
