@extends('layouts.master')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/defaultBackground.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

@endsection

@section('container')
    <div class="wrapper">

        @include('client.layouts.navbar')

        @yield('orderContainer')

        @include('client.layouts.footer')
    </div>
@endsection
