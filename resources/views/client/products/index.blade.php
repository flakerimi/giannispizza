@extends('client.layouts.master')

@section('orderContainer')

    <div class="container">
		<div class="well well-sm">
            <h4>Products</h4>
        </div>
        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-pills nav-stacked nav-left">
                        <li role="presentation"><a href="">Categories</a></li>
                        <li role="presentation"><a href="{{ url('product/show/pizza') }}">Pizza</a></li>
                        <li role="presentation"><a href="{{ url('product/show/pizza') }}">Salad</a></li>
                        <li role="presentation"><a href="{{ url('product/show/pizza') }}">Fish</a></li>
                </ul>
            </div>
            <div class="col-md-10">
		       <div class="row">
               @if (count($products) > 0)
                    @foreach ($products as $product)
                        <div class="col-md-4">
                            <div class="panel">
                                <div class="panel-body">
                                    <img class="img-responsive" src="{{ asset('img/pizza-margarita.jpg') }}" alt="">
                                    <form action="{{ url('/order') }}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="product" value="{{ $product->id }}">
                                        <input type="hidden" name="price" value="{{ $product->round_price }}">
                                        <a href="{{ url('product/show') }}/{{ $product->id }}">
                                            <h4>{{ $product->name }} </h4>
                                        </a>
                                        <div class="row" style="margin-bottom: 15px;margin-top: 15px;">
                                            <div class="col-md-9">
                                                <strong>Ingredients:</strong><br>
                                                <p>
                                                    @foreach ($product->attributes as $attribute)
                                                        {{ $attribute->name }}
                                                            @if($loop->count != $loop->last)
                                                                ,
                                                            @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <strong>Price:</strong><br>
                                                 <span>€ </span>{{ $product->round_price }}
                                            </div>
                                        </div>
                                        <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-success btn-md btn-block"> ORDER NOW</input>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                 @else
                     <div class="">
                        <h3>There is no product!</h3>
                     </div>
                 @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jquery_part')
    <script>
        $(document).ready(function(){

        });
    </script>
@endsection
