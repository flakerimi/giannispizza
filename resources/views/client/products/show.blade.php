@extends('client.layouts.master')

@section('orderContainer')

    <div class="container">
		<div class="well well-sm">
            <h4>Products</h4>
        </div>
        <div class="row">
			<form action="{{ url('/order') }}" method="post">
			<h4 class="text-center">{{ $product->name }}</h4>
            <div class="col-md-6">
                    <img class="img-responsive" src="{{ asset('img/pizza-margarita.jpg') }}" alt="">
                        {{ csrf_field() }}
                        <input type="hidden" name="product" value="{{ $product->id }}">
                        <input type="hidden" name="price" value="{{ $product->round_price }}">

			</div>
			<div class="col-md-6">
                <div class="row" style="margin-bottom: 15px;margin-top: 15px;">
                    <div class="col-md-9">
                        <strong>Ingredients:</strong><br>
                        <p>
                            @foreach ($product->attributes as $attribute)
                                {{ $attribute->name }}
                                    @if($loop->count != $loop->last)
                                        ,
                                    @endif
                            @endforeach
                        </p>
                    </div>
                    <div class="col-md-3">
                        <strong>Price:</strong><br>
                         <span>€ </span>{{ $product->round_price }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success btn-md btn-block"> ORDER NOW</input>
                    </div>
	            </div>
	        </div>
		</form>
    </div>
</div>

@endsection

@section('jquery_part')
    <script>
        $(document).ready(function(){

        });
    </script>
@endsection
