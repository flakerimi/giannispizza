@extends('client.layouts.master')

@section('orderContainer')
    <div class="container-fluid">
        <div class="well well-sm">
            <a href="{{ url('/order/custom/product')}}" class="btn btn-warning pull-right" type="button" name="button">Create</a>
            <h4 class="text-left">Create your own pizza </h4>
        </div>
    </div>
    <div class="background-image">
        <div class="headline">
            <h2>Pizza Delivery Near Me</h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach ($products as $product)
                <div class="col-md-4">
                    <div class="panel">
                        <div class="panel-body">
                            <img class="img-responsive" src="{{ asset('img/pizza-margarita.jpg') }}" alt="">
                            <form action="{{ url('/order') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="product" value="{{ $product->id }}">
                                <input type="hidden" name="price" value="{{ $product->round_price }}">
                                <a href="{{ url('product/show') }}/{{ $product->id }}">
                                    <h4>{{ $product->name }} </h4>
                                </a>
                                <div class="row ingredients-row">
                                    <div class="col-md-9">
                                      <button type="submit" class="btn btn-success btn-md btn-block"> ORDER NOW</input>
                                        {{-- <strong>Ingredients:</strong><br>
                                        <p>
                                            @foreach ($product->attributes as $attribute)
                                                {{ $attribute->name }}
                                                    @if($loop->count != $loop->last)
                                                        ,
                                                    @endif
                                            @endforeach
                                        </p> --}}
                                    </div>
                                    <div class="col-md-3">
                                        <strong>Price:</strong><br>
                                        <strong>
                                            <small>€ </small>
                                            {{ $product->round_price }}
                                        </strong>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-md-12">
                                        
                                    </div>
                                </div> --}}
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('jquery_part')
    <script>
        $(document).ready(function(){

        });
    </script>
@endsection
