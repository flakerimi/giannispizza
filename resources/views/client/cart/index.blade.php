@extends('client.layouts.master')

@section('orderContainer')

    <div class="container">

        <div class="well" style="border-radius: 0px; ">
            <div class="row">

                <div class="pull-left ">
                    <a href="{{ url('/order') }}" class="btn btn-info" style="margin-left: 13px;border-radius: 0px; background: #434343; border: 1px solid #434343;">
                        <i class="fa fa-arrow-left"> </i>
                            Back to products
                    </a>
                        <a href="#" class="btn btn-info" style="border-radius: 0px; margin-left: 15px;">
                        <i class="fa fa-trash-o"></i> Clear order</a>
                </div>
                <div class="pull-right">

                    <a href="#" class="btn"
                        data-toggle="modal" data-target="#mapModal"
                        style="border-radius: 0px; background: #e45227; color: white; border: 1px solid #e45227; margin-right: 13px;">
                        Continue to order
                        <i class="fa fa-arrow-right"> </i>
                    </a>
                </div>
            </div>
        </div>

        @if(count($orders) > 0)
            <table class="table ">
                <thead>
                    <tr style="background: #d3d3d3;">
                        <th width="20%">Product</th>
                        <th width="35%"></th>
                        <th width="10%">Price</th>
                        <th width="15%">Quantity</th>
                        <th width="15%">Total price</th>
                        <th width="5%"></th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($orders as $order)
                    <tr>
                      <td>
                            <img src="{{ asset('img/pizza-palma.png') }}" width="200px" alt="">
                        </td>
                        <td>
                            {{ $order->orderitem->product->name }}
                        </td>
                        <td>
                            <span>€</span>
                            <span class="price">
                              {{ $order->orderitem->price }}
                            </span>
                        </td>
                        <td>
                            <div class="form-group">
                                <button id="decrease" style="border:1px solid #d3d3d3;">-</button>
                                <input type="text" class="text-center counter-value" value="1" style="width:50px; border:1px solid #d3d3d3;">
                                <button id="increase" style="border:1px solid #d3d3d3;">+</button>
                            </div>
                        </td>
                        <td>
                            <span>€</span>
                            <span class="totalPrice"></span>
                            <input type="hidden" class="totalPrice" >
                        </td>
                        <td>
                            <a href="{{ url('order/delete')}}/{{ $order->id }}" type="button" name="button" class="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        @else

            <h4 class="text-center">There is no product selected</h4>
        @endif
    </div>
    @include('client.address.map_modal')
@endsection

@section('jquery_part')
    <script>
        $(document).ready(function(){
            var counterValue = $('.counter-value');
            var totalPrice = $('.totalPrice');
            var price = $('.price').text();
            var count = counterValue.val();

            totalPrice.text(price);
            $('#decrease').click(function(){
                count--;
                if(count <= 0)
                    count = 1;

                counterValue.val(count);
                totalPrice.text((price * count).toFixed(2));
            });
            $('#increase').click(function(){
                count++;
                counterValue.val(count);
               totalPrice.text((price * count).toFixed(2));
            });
        });
    </script>
@endsection
