<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.head')
    @yield('style')
</head>

<body>

    @yield('container')

@include('partials.javascripts')

@yield('jquery_part')
</body>
</html>
