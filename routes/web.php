<?php
Route::get('/', function () { return redirect('/admin'); });

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

// Registration Routes..
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
$this->post('register', 'Auth\RegisterController@register')->name('auth.register');

// Social Login Routes..
Route::get('login/{driver}', 'Auth\LoginController@redirectToSocial')->name('auth.login.social');
Route::get('{driver}/callback', 'Auth\LoginController@handleSocialCallback')->name('auth.login.social_callback');


// client part of authenication
Route::group(['middleware' => ['auth']], function(){

    Route::get('/order', 'OrderController@index');
    // Part of fillind address and order product with pasion
    Route::get('/order', 'OrderController@index');
    Route::post('/order', 'OrderController@order');
    Route::get('/myorders', 'OrderController@showOrders');

    Route::post('/order/confirm', 'OrderController@confirmOrder');
    Route::get('/order/delete/{id}', 'OrderController@destroy');
    Route::get('/cart', 'OrderController@getOrderCart');

    Route::get('/order/custom/product','OrderController@customPizza');
    Route::post('/order/store/custom', 'OrderController@storeOrder');
     // End of orders

    // Part of products
    Route::get('/product', 'ProductsController@index');
    Route::get('/product/show/{id}', 'ProductsController@show');

    //ends of products

    //addresses
    Route::get('/address', 'AddressController@index');
    Route::get('/address/create', 'AddressController@create');
    Route::post('/address/store', 'AddressController@add');
    Route::get('/address/delete/{id}', 'AddressController@destroy');
});

// admin part of authentication
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/', function () {
            return redirect('/admin/home');
    });

    Route::get('/', 'HomeController@index');
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('user_actions', 'Admin\UserActionsController');
    Route::get('internal_notifications/read', 'InternalNotificationsController@read');
    Route::resource('internal_notifications', 'Admin\InternalNotificationsController');
    Route::post('internal_notifications_mass_destroy', ['uses' => 'Admin\InternalNotificationsController@massDestroy', 'as' => 'internal_notifications.mass_destroy']);
    Route::resource('attributes', 'Admin\AttributesController');
    Route::post('attributes_mass_destroy', ['uses' => 'Admin\AttributesController@massDestroy', 'as' => 'attributes.mass_destroy']);
    Route::resource('attribute_items', 'Admin\AttributeItemsController');
    Route::post('attribute_items_mass_destroy', ['uses' => 'Admin\AttributeItemsController@massDestroy', 'as' => 'attribute_items.mass_destroy']);
    Route::resource('products', 'Admin\ProductsController');
    Route::post('products_mass_destroy', ['uses' => 'Admin\ProductsController@massDestroy', 'as' => 'products.mass_destroy']);
    Route::resource('orders', 'Admin\OrdersController');
    Route::post('orders_mass_destroy', ['uses' => 'Admin\OrdersController@massDestroy', 'as' => 'orders.mass_destroy']);
    Route::resource('addresses', 'Admin\AddressesController');
    Route::post('addresses_mass_destroy', ['uses' => 'Admin\AddressesController@massDestroy', 'as' => 'addresses.mass_destroy']);
    Route::resource('orderitems', 'Admin\OrderitemsController');
    Route::post('orderitems_mass_destroy', ['uses' => 'Admin\OrderitemsController@massDestroy', 'as' => 'orderitems.mass_destroy']);

    Route::model('messenger', 'App\MessengerTopic');
    Route::get('messenger/inbox', 'MessengerController@inbox')->name('messenger.inbox');
    Route::get('messenger/outbox', 'MessengerController@outbox')->name('messenger.outbox');
    Route::resource('messenger', 'MessengerController');
});
