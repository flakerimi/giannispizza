<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {

        Route::resource('attributes', 'AttributesController');

        Route::resource('attribute_items', 'AttributeItemsController');

        Route::resource('products', 'ProductsController');

        Route::resource('orderitems', 'OrderitemsController');

});
