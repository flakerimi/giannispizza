<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Address;

class AddressController extends Controller
{
    public function index()
    {
        $addresses = Address::all();

        return view('client.address.index', compact('addresses'));
    }

    public function create(Request $request)
    {
        return view('client.address.create');
    }

    public function add(Request $request)
    {

        Address::create([
            'address' => $request->input('address'),
            'city' => $request->input('city'),
            'latitude' => $request->input('latitude'),
            'longtitude' => $request->input('longtitude'),
            'client_id' => auth()->user()->id
        ]);

        return "ok";
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $address = Address::find($id);
        $address->delete();

        return redirect('address');
    }
}
