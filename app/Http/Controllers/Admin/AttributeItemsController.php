<?php

namespace App\Http\Controllers\Admin;

use App\AttributeItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAttributeItemsRequest;
use App\Http\Requests\Admin\UpdateAttributeItemsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class AttributeItemsController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of AttributeItem.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('attribute_item_access')) {
            return abort(401);
        }

        $attribute_items = AttributeItem::all();

        return view('admin.attribute_items.index', compact('attribute_items'));
    }

    /**
     * Show the form for creating new AttributeItem.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('attribute_item_create')) {
            return abort(401);
        }
        $attributes = \App\Attribute::get()->pluck('name', 'id');

        return view('admin.attribute_items.create', compact('attributes'));
    }

    /**
     * Store a newly created AttributeItem in storage.
     *
     * @param  \App\Http\Requests\StoreAttributeItemsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAttributeItemsRequest $request)
    {
        if (! Gate::allows('attribute_item_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $attribute_item = AttributeItem::create($request->all());
        $attribute_item->attribute()->sync(array_filter((array)$request->input('attribute')));



        return redirect()->route('admin.attribute_items.index');
    }


    /**
     * Show the form for editing AttributeItem.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('attribute_item_edit')) {
            return abort(401);
        }
        $attributes = \App\Attribute::get()->pluck('name', 'id');

        $attribute_item = AttributeItem::findOrFail($id);

        return view('admin.attribute_items.edit', compact('attribute_item', 'attributes'));
    }

    /**
     * Update AttributeItem in storage.
     *
     * @param  \App\Http\Requests\UpdateAttributeItemsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAttributeItemsRequest $request, $id)
    {
        if (! Gate::allows('attribute_item_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $attribute_item = AttributeItem::findOrFail($id);
        $attribute_item->update($request->all());
        $attribute_item->attribute()->sync(array_filter((array)$request->input('attribute')));



        return redirect()->route('admin.attribute_items.index');
    }


    /**
     * Display AttributeItem.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('attribute_item_view')) {
            return abort(401);
        }
        $attributes = \App\Attribute::get()->pluck('name', 'id');$products = \App\Product::whereHas('attributes',
                    function ($query) use ($id) {
                        $query->where('id', $id);
                    })->get();$orders = \App\Order::where('status_id', $id)->get();$products = \App\Product::whereHas('primary_ingredients',
                    function ($query) use ($id) {
                        $query->where('id', $id);
                    })->get();$products = \App\Product::where('secondary_ingredients_id', $id)->get();

        $attribute_item = AttributeItem::findOrFail($id);

        return view('admin.attribute_items.show', compact('attribute_item', 'products', 'orders', 'products', 'products'));
    }


    /**
     * Remove AttributeItem from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('attribute_item_delete')) {
            return abort(401);
        }
        $attribute_item = AttributeItem::findOrFail($id);
        $attribute_item->delete();

        return redirect()->route('admin.attribute_items.index');
    }

    /**
     * Delete all selected AttributeItem at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('attribute_item_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = AttributeItem::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
