<?php

namespace App\Http\Controllers\Admin;

use App\Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAttributesRequest;
use App\Http\Requests\Admin\UpdateAttributesRequest;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class AttributesController extends Controller
{
    /**
     * Display a listing of Attribute.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('attribute_access')) {
            return abort(401);
        }

        $attributes = Attribute::all();

        return view('admin.attributes.index', compact('attributes'));
    }

    /**
     * Show the form for creating new Attribute.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('attribute_create')) {
            return abort(401);
        }
        return view('admin.attributes.create');
    }

    /**
     * Store a newly created Attribute in storage.
     *
     * @param  \App\Http\Requests\StoreAttributesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAttributesRequest $request)
    {
        if (! Gate::allows('attribute_create')) {
            return abort(401);
        }
        $attribute = Attribute::create($request->all());



        return redirect()->route('admin.attributes.index');
    }


    /**
     * Show the form for editing Attribute.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('attribute_edit')) {
            return abort(401);
        }
        $attribute = Attribute::findOrFail($id);

        return view('admin.attributes.edit', compact('attribute'));
    }

    /**
     * Update Attribute in storage.
     *
     * @param  \App\Http\Requests\UpdateAttributesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAttributesRequest $request, $id)
    {
        if (! Gate::allows('attribute_edit')) {
            return abort(401);
        }
        $attribute = Attribute::findOrFail($id);
        $attribute->update($request->all());



        return redirect()->route('admin.attributes.index');
    }


    /**
     * Display Attribute.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('attribute_view')) {
            return abort(401);
        }
        $attribute_items = \App\AttributeItem::whereHas('attribute',
                    function ($query) use ($id) {
                        $query->where('id', $id);
                    })->get();

        $attribute = Attribute::findOrFail($id);

        return view('admin.attributes.show', compact('attribute', 'attribute_items'));
    }


    /**
     * Remove Attribute from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('attribute_delete')) {
            return abort(401);
        }
        $attribute = Attribute::findOrFail($id);
        $attribute->delete();

        return redirect()->route('admin.attributes.index');
    }

    /**
     * Delete all selected Attribute at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('attribute_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Attribute::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
