<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProductsRequest;
use App\Http\Requests\Admin\UpdateProductsRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class ProductsController extends Controller
{
    /**
     * Display a listing of Product.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('product_access')) {
            return abort(401);
        }
        if ($filterBy = Input::get('filter')) {
            if ($filterBy == 'all') {
                Session::put('Product.filter', 'all');
            } elseif ($filterBy == 'my') {
                Session::put('Product.filter', 'my');
            }
        }
        
        if (request()->ajax()) {
            $query = Product::query();
            $query->with("attributes");
            $query->with("primary_ingredients");
            $query->with("secondary_ingredients");
            $query->with("user");
            $table = Datatables::of($query);
            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) {
                $gateKey  = 'product_';
                $routeKey = 'admin.products';

                return view('actionsTemplate', compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : '';
            });
            $table->editColumn('round_price', function ($row) {
                return $row->round_price ? $row->round_price : '';
            });
            $table->editColumn('attributes.name', function ($row) {
                if(count($row->attributes) == 0) {
                    return '';
                }
                
                return '<span class="label label-info label-many">' . implode('</span><span class="label label-info label-many"> ',
                        $row->attributes->pluck('name')->toArray()) . '</span>';
            });
            $table->editColumn('primary_ingredients.name', function ($row) {
                if(count($row->primary_ingredients) == 0) {
                    return '';
                }
                
                return '<span class="label label-info label-many">' . implode('</span><span class="label label-info label-many"> ',
                        $row->primary_ingredients->pluck('name')->toArray()) . '</span>';
            });
            $table->editColumn('secondary_ingredients.name', function ($row) {
                return $row->secondary_ingredients ? $row->secondary_ingredients->name : '';
            });
            $table->editColumn('user.name', function ($row) {
                return $row->user ? $row->user->name : '';
            });

            return $table->make(true);
        }

        return view('admin.products.index');
    }

    /**
     * Show the form for creating new Product.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('product_create')) {
            return abort(401);
        }
        $attributes = \App\AttributeItem::get()->pluck('name', 'id');$primary_ingredients = \App\AttributeItem::get()->pluck('name', 'id');$secondary_ingredients = \App\AttributeItem::get()->pluck('name', 'id')->prepend('Please select', '');$users = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');

        return view('admin.products.create', compact('attributes', 'primary_ingredients', 'secondary_ingredients', 'users'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param  \App\Http\Requests\StoreProductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductsRequest $request)
    {
        if (! Gate::allows('product_create')) {
            return abort(401);
        }
        $product = Product::create($request->all());
        $product->attributes()->sync(array_filter((array)$request->input('attributes')));
        $product->primary_ingredients()->sync(array_filter((array)$request->input('primary_ingredients')));



        return redirect()->route('admin.products.index');
    }


    /**
     * Show the form for editing Product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('product_edit')) {
            return abort(401);
        }
        $attributes = \App\AttributeItem::get()->pluck('name', 'id');$primary_ingredients = \App\AttributeItem::get()->pluck('name', 'id');$secondary_ingredients = \App\AttributeItem::get()->pluck('name', 'id')->prepend('Please select', '');$users = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');

        $product = Product::findOrFail($id);

        return view('admin.products.edit', compact('product', 'attributes', 'primary_ingredients', 'secondary_ingredients', 'users'));
    }

    /**
     * Update Product in storage.
     *
     * @param  \App\Http\Requests\UpdateProductsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductsRequest $request, $id)
    {
        if (! Gate::allows('product_edit')) {
            return abort(401);
        }
        $product = Product::findOrFail($id);
        $product->update($request->all());
        $product->attributes()->sync(array_filter((array)$request->input('attributes')));
        $product->primary_ingredients()->sync(array_filter((array)$request->input('primary_ingredients')));



        return redirect()->route('admin.products.index');
    }


    /**
     * Display Product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('product_view')) {
            return abort(401);
        }
        $attributes = \App\AttributeItem::get()->pluck('name', 'id');$primary_ingredients = \App\AttributeItem::get()->pluck('name', 'id');$secondary_ingredients = \App\AttributeItem::get()->pluck('name', 'id')->prepend('Please select', '');$users = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');$orderitems = \App\Orderitem::where('product_id', $id)->get();

        $product = Product::findOrFail($id);

        return view('admin.products.show', compact('product', 'orderitems'));
    }


    /**
     * Remove Product from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('product_delete')) {
            return abort(401);
        }
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect()->route('admin.products.index');
    }

    /**
     * Delete all selected Product at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('product_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Product::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
