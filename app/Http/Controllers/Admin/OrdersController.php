<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreOrdersRequest;
use App\Http\Requests\Admin\UpdateOrdersRequest;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class OrdersController extends Controller
{
    /**
     * Display a listing of Order.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('order_access')) {
            return abort(401);
        }

        
        if (request()->ajax()) {
            $query = Order::query();
            $query->with("client");
            $query->with("status");
            $query->with("delivered_by");
            $query->with("address");
            $table = Datatables::of($query);
            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) {
                $gateKey  = 'order_';
                $routeKey = 'admin.orders';

                return view('actionsTemplate', compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('client.name', function ($row) {
                return $row->client ? $row->client->name : '';
            });
            $table->editColumn('date', function ($row) {
                return $row->date ? $row->date : '';
            });
            $table->editColumn('status.name', function ($row) {
                return $row->status ? $row->status->name : '';
            });
            $table->editColumn('delivered_by.name', function ($row) {
                return $row->delivered_by ? $row->delivered_by->name : '';
            });
            $table->editColumn('address.address', function ($row) {
                return $row->address ? $row->address->address : '';
            });

            return $table->make(true);
        }

        return view('admin.orders.index');
    }

    /**
     * Show the form for creating new Order.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('order_create')) {
            return abort(401);
        }
        $clients = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');$statuses = \App\AttributeItem::get()->pluck('name', 'id')->prepend('Please select', '');$delivered_bies = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');$addresses = \App\Address::get()->pluck('address', 'id')->prepend('Please select', '');

        return view('admin.orders.create', compact('clients', 'statuses', 'delivered_bies', 'addresses'));
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param  \App\Http\Requests\StoreOrdersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrdersRequest $request)
    {
        if (! Gate::allows('order_create')) {
            return abort(401);
        }
        $order = Order::create($request->all());



        return redirect()->route('admin.orders.index');
    }


    /**
     * Show the form for editing Order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('order_edit')) {
            return abort(401);
        }
        $clients = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');$statuses = \App\AttributeItem::get()->pluck('name', 'id')->prepend('Please select', '');$delivered_bies = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');$addresses = \App\Address::get()->pluck('address', 'id')->prepend('Please select', '');

        $order = Order::findOrFail($id);

        return view('admin.orders.edit', compact('order', 'clients', 'statuses', 'delivered_bies', 'addresses'));
    }

    /**
     * Update Order in storage.
     *
     * @param  \App\Http\Requests\UpdateOrdersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrdersRequest $request, $id)
    {
        if (! Gate::allows('order_edit')) {
            return abort(401);
        }
        $order = Order::findOrFail($id);
        $order->update($request->all());



        return redirect()->route('admin.orders.index');
    }


    /**
     * Display Order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('order_view')) {
            return abort(401);
        }
        $clients = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');$statuses = \App\AttributeItem::get()->pluck('name', 'id')->prepend('Please select', '');$delivered_bies = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');$addresses = \App\Address::get()->pluck('address', 'id')->prepend('Please select', '');$orderitems = \App\Orderitem::where('order_id', $id)->get();

        $order = Order::findOrFail($id);

        return view('admin.orders.show', compact('order', 'orderitems'));
    }


    /**
     * Remove Order from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('order_delete')) {
            return abort(401);
        }
        $order = Order::findOrFail($id);
        $order->delete();

        return redirect()->route('admin.orders.index');
    }

    /**
     * Delete all selected Order at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('order_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Order::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
