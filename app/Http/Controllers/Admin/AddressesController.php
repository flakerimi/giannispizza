<?php

namespace App\Http\Controllers\Admin;

use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAddressesRequest;
use App\Http\Requests\Admin\UpdateAddressesRequest;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class AddressesController extends Controller
{
    /**
     * Display a listing of Address.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('address_access')) {
            return abort(401);
        }

        
        if (request()->ajax()) {
            $query = Address::query();
            $query->with("client");
            $table = Datatables::of($query);
            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) {
                $gateKey  = 'address_';
                $routeKey = 'admin.addresses';

                return view('actionsTemplate', compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('address', function ($row) {
                return $row->address ? $row->address : '';
            });
            $table->editColumn('city', function ($row) {
                return $row->city ? $row->city : '';
            });
            $table->editColumn('longtitude', function ($row) {
                return $row->longtitude ? $row->longtitude : '';
            });
            $table->editColumn('latitude', function ($row) {
                return $row->latitude ? $row->latitude : '';
            });
            $table->editColumn('client.name', function ($row) {
                return $row->client ? $row->client->name : '';
            });

            return $table->make(true);
        }

        return view('admin.addresses.index');
    }

    /**
     * Show the form for creating new Address.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('address_create')) {
            return abort(401);
        }
        $clients = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');

        return view('admin.addresses.create', compact('clients'));
    }

    /**
     * Store a newly created Address in storage.
     *
     * @param  \App\Http\Requests\StoreAddressesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAddressesRequest $request)
    {
        if (! Gate::allows('address_create')) {
            return abort(401);
        }
        $address = Address::create($request->all());



        return redirect()->route('admin.addresses.index');
    }


    /**
     * Show the form for editing Address.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('address_edit')) {
            return abort(401);
        }
        $clients = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');

        $address = Address::findOrFail($id);

        return view('admin.addresses.edit', compact('address', 'clients'));
    }

    /**
     * Update Address in storage.
     *
     * @param  \App\Http\Requests\UpdateAddressesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAddressesRequest $request, $id)
    {
        if (! Gate::allows('address_edit')) {
            return abort(401);
        }
        $address = Address::findOrFail($id);
        $address->update($request->all());



        return redirect()->route('admin.addresses.index');
    }


    /**
     * Display Address.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('address_view')) {
            return abort(401);
        }
        $clients = \App\User::get()->pluck('name', 'id')->prepend('Please select', '');$orders = \App\Order::where('address_id', $id)->get();

        $address = Address::findOrFail($id);

        return view('admin.addresses.show', compact('address', 'orders'));
    }


    /**
     * Remove Address from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('address_delete')) {
            return abort(401);
        }
        $address = Address::findOrFail($id);
        $address->delete();

        return redirect()->route('admin.addresses.index');
    }

    /**
     * Delete all selected Address at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('address_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Address::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
