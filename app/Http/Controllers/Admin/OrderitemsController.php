<?php

namespace App\Http\Controllers\Admin;

use App\Orderitem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreOrderitemsRequest;
use App\Http\Requests\Admin\UpdateOrderitemsRequest;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class OrderitemsController extends Controller
{
    /**
     * Display a listing of Orderitem.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('orderitem_access')) {
            return abort(401);
        }

        $orderitems = Orderitem::all();

        return view('admin.orderitems.index', compact('orderitems'));
    }

    /**
     * Show the form for creating new Orderitem.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('orderitem_create')) {
            return abort(401);
        }
        $products = \App\Product::get()->pluck('name', 'id')->prepend('Please select', '');$orders = \App\Order::get()->pluck('date', 'id')->prepend('Please select', '');

        return view('admin.orderitems.create', compact('products', 'orders'));
    }

    /**
     * Store a newly created Orderitem in storage.
     *
     * @param  \App\Http\Requests\StoreOrderitemsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderitemsRequest $request)
    {
        if (! Gate::allows('orderitem_create')) {
            return abort(401);
        }
        $orderitem = Orderitem::create($request->all());



        return redirect()->route('admin.orderitems.index');
    }


    /**
     * Show the form for editing Orderitem.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('orderitem_edit')) {
            return abort(401);
        }
        $products = \App\Product::get()->pluck('name', 'id')->prepend('Please select', '');$orders = \App\Order::get()->pluck('date', 'id')->prepend('Please select', '');

        $orderitem = Orderitem::findOrFail($id);

        return view('admin.orderitems.edit', compact('orderitem', 'products', 'orders'));
    }

    /**
     * Update Orderitem in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderitemsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderitemsRequest $request, $id)
    {
        if (! Gate::allows('orderitem_edit')) {
            return abort(401);
        }
        $orderitem = Orderitem::findOrFail($id);
        $orderitem->update($request->all());



        return redirect()->route('admin.orderitems.index');
    }


    /**
     * Display Orderitem.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('orderitem_view')) {
            return abort(401);
        }
        $orderitem = Orderitem::findOrFail($id);

        return view('admin.orderitems.show', compact('orderitem'));
    }


    /**
     * Remove Orderitem from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('orderitem_delete')) {
            return abort(401);
        }
        $orderitem = Orderitem::findOrFail($id);
        $orderitem->delete();

        return redirect()->route('admin.orderitems.index');
    }

    /**
     * Delete all selected Orderitem at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('orderitem_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Orderitem::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
