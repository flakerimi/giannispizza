<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Order;
use App\Orderitem;

use App\Product;
use App\Attribute;
use App\AttributeItem;

use App\Address;

use Carbon\Carbon;

class OrderController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $attributes = Attribute::all()->pluck('name');

        return view('client.index', compact('products','attributes'));
    }


    public function order(Request $request)
    {
        $date = Carbon::now();
        $order = Order::create([
          'client_id' => auth()->user()->id,
          'status_id' => 8
        ]);

        $orderItems = Orderitem::create([
            'product_id' => $request->input('product'),
            'price' =>  $request->input('price'),
            'order_id' => $order->id
        ]);

        return redirect('/cart');
    }

    public function customPizza()
    {
        $ingredients = AttributeItem::all();

        return view('client.customOrder', compact('ingredients'));
    }

    public function getOrderCart()
    {
        $orders = Order::where('client_id', '=', auth()->user()->id)
                ->where('date', '=', NULL)
                ->where('status_id', '=', 8)
                ->get();

        $address = Address::where('client_id', '=', auth()->user()->id)
                    ->get();

        return view('client.cart.index', compact('orders', 'address'));
    }

    public function confirmOrder(Request $request)
    {
            $order = Order::where('client_id', '=', auth()->user()->id)
                ->where('status_id', '=', 8)
                ->update([
                    'address_id' => $request->input('address'),
                    'status_id' => 9
                ]);

            return redirect('cart');
    }

    public function showOrders()
    {
        $orders = Order::where('client_id', '=', auth()->user()->id)
                ->get();
        return view('client.orders.orders', compact('orders'));
    }

    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();

        return redirect('/cart');
    }
}
