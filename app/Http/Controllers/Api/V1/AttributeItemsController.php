<?php

namespace App\Http\Controllers\Api\V1;

use App\AttributeItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAttributeItemsRequest;
use App\Http\Requests\Admin\UpdateAttributeItemsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class AttributeItemsController extends Controller
{
    use FileUploadTrait;

    public function index()
    {
        return AttributeItem::all();
    }

    public function show($id)
    {
        return AttributeItem::findOrFail($id);
    }

    public function update(UpdateAttributeItemsRequest $request, $id)
    {
        $request = $this->saveFiles($request);
        $attribute_item = AttributeItem::findOrFail($id);
        $attribute_item->update($request->all());
        

        return $attribute_item;
    }

    public function store(StoreAttributeItemsRequest $request)
    {
        $request = $this->saveFiles($request);
        $attribute_item = AttributeItem::create($request->all());
        

        return $attribute_item;
    }

    public function destroy($id)
    {
        $attribute_item = AttributeItem::findOrFail($id);
        $attribute_item->delete();
        return '';
    }
}
