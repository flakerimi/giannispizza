<?php

namespace App\Http\Controllers\Api\V1;

use App\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAttributesRequest;
use App\Http\Requests\Admin\UpdateAttributesRequest;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class AttributesController extends Controller
{
    public function index()
    {
        return Attribute::all();
    }

    public function show($id)
    {
        return Attribute::findOrFail($id);
    }

    public function update(UpdateAttributesRequest $request, $id)
    {
        $attribute = Attribute::findOrFail($id);
        $attribute->update($request->all());
        

        return $attribute;
    }

    public function store(StoreAttributesRequest $request)
    {
        $attribute = Attribute::create($request->all());
        

        return $attribute;
    }

    public function destroy($id)
    {
        $attribute = Attribute::findOrFail($id);
        $attribute->delete();
        return '';
    }
}
