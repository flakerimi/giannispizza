<?php

namespace App\Http\Controllers\Api\V1;

use App\Orderitem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreOrderitemsRequest;
use App\Http\Requests\Admin\UpdateOrderitemsRequest;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class OrderitemsController extends Controller
{
    public function index()
    {
        return Orderitem::all();
    }

    public function show($id)
    {
        return Orderitem::findOrFail($id);
    }

    public function update(UpdateOrderitemsRequest $request, $id)
    {
        $orderitem = Orderitem::findOrFail($id);
        $orderitem->update($request->all());
        

        return $orderitem;
    }

    public function store(StoreOrderitemsRequest $request)
    {
        $orderitem = Orderitem::create($request->all());
        

        return $orderitem;
    }

    public function destroy($id)
    {
        $orderitem = Orderitem::findOrFail($id);
        $orderitem->delete();
        return '';
    }
}
