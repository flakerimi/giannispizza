<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'attributes' => 'required',
            'attributes.*' => 'exists:attribute_items,id',
            'primary_ingredients.*' => 'exists:attribute_items,id',
            'user_id' => 'required',
        ];
    }
}
