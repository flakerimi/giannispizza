<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Attribute
 *
 * @package App
 * @property string $name
 * @property string $slug
*/
class Attribute extends Model
{
    protected $fillable = ['name', 'slug'];

    public static function boot()
    {
        parent::boot();

        Attribute::observe(new \App\Observers\UserActionsObserver);
    }

    // public function getNameAttribute($value)
    // {
    //
    //     $value = explode(' ', $value);
    //     return ucfirst($value[0]);
    // }

}
