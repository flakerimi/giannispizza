<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Address
 *
 * @package App
 * @property text $address
 * @property string $city
 * @property string $longtitude
 * @property string $latitude
 * @property string $client
*/
class Address extends Model
{
    protected $fillable = ['address', 'city', 'longtitude', 'latitude', 'client_id'];
    
    public static function boot()
    {
        parent::boot();

        Address::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setClientIdAttribute($input)
    {
        $this->attributes['client_id'] = $input ? $input : null;
    }
    
    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }
    
}
