<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Orderitem
 *
 * @package App
 * @property string $product
 * @property decimal $price
 * @property string $order
*/
class Orderitem extends Model
{
    protected $fillable = ['price', 'product_id', 'order_id'];
    
    public static function boot()
    {
        parent::boot();

        Orderitem::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setProductIdAttribute($input)
    {
        $this->attributes['product_id'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setPriceAttribute($input)
    {
        $this->attributes['price'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setOrderIdAttribute($input)
    {
        $this->attributes['order_id'] = $input ? $input : null;
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id')->withTrashed();
    }
    
}
