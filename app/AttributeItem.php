<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AttributeItem
 *
 * @package App
 * @property string $name
 * @property string $value
 * @property string $photo
*/
class AttributeItem extends Model
{
    protected $fillable = ['name', 'value', 'photo'];
    
    public static function boot()
    {
        parent::boot();

        AttributeItem::observe(new \App\Observers\UserActionsObserver);
    }
    
    public function attribute()
    {
        return $this->belongsToMany(Attribute::class, 'attribute_attribute_item');
    }
    
}
