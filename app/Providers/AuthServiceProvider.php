<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $user = \Auth::user();


        Gate::define('orders_product', function($user){
            return in_array($user->role_id, [1,2,3]);
        });


        // Auth gates for: User management
        Gate::define('user_management_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Roles
        Gate::define('role_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Users
        Gate::define('user_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: User actions
        Gate::define('user_action_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Internal notifications
        Gate::define('internal_notification_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('internal_notification_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('internal_notification_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('internal_notification_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('internal_notification_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Catalogue
        Gate::define('catalogue_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });

        // Auth gates for: Attributes
        Gate::define('attribute_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('attribute_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('attribute_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('attribute_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('attribute_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Attribute items
        Gate::define('attribute_item_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('attribute_item_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('attribute_item_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('attribute_item_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('attribute_item_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Products
        Gate::define('product_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('product_create', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('product_edit', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('product_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('product_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Sales
        Gate::define('sale_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });

        // Auth gates for: Orders
        Gate::define('order_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('order_create', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('order_edit', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('order_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('order_delete', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });

        // Auth gates for: Addresses
        Gate::define('address_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('address_create', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('address_edit', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('address_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('address_delete', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });

        // Auth gates for: Orderitems
        Gate::define('orderitem_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('orderitem_create', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('orderitem_edit', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('orderitem_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('orderitem_delete', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });

    }
}
