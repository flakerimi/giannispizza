<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
//use App\Traits\FilterByUser;

/**
 * Class Product
 *
 * @package App
 * @property string $name
 * @property integer $round_price
 * @property string $secondary_ingredients
 * @property string $user
*/
class Product extends Model
{
    //use FilterByUser;

    protected $fillable = ['name', 'round_price', 'secondary_ingredients_id', 'user_id'];

    public static function boot()
    {
        parent::boot();

        Product::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setRoundPriceAttribute($input)
    {
        $this->attributes['round_price'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setSecondaryIngredientsIdAttribute($input)
    {
        $this->attributes['secondary_ingredients_id'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setUserIdAttribute($input)
    {
        $this->attributes['user_id'] = $input ? $input : null;
    }

    public function attributes()
    {
        return $this->belongsToMany(AttributeItem::class, 'attribute_item_product');
    }

    public function primary_ingredients()
    {
        return $this->belongsToMany(AttributeItem::class, 'attribute_item_product');
    }

    public function secondary_ingredients()
    {
        return $this->belongsTo(AttributeItem::class, 'secondary_ingredients_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
